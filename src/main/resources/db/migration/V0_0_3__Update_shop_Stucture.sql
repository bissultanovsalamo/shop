create table shop.category_products
(
    id            serial primary key,
    name_category varchar(255),
    icon_base64   text
);