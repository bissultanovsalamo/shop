

create table shop.products
(
    id            serial primary key,
    category_id   integer,
    icon_base64   text,
    name_product  varchar(1000),
    brand_product varchar(1000),
    description   varchar(1000),
    price         integer,
    count         integer,
    created_date  timestamp,
    foreign key (category_id) references shop.category_products (id)
);



create table shop.basket
(
    id         serial primary key,
    product_id integer,
    user_id    integer,
    foreign key (product_id) references shop.products (id),
    foreign key (user_id) references shop.users (id)
);