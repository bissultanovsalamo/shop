create table shop.shop (id serial primary key, name varchar(255), address varchar(255));

CREATE TABLE shop.users (id serial primary key, name varchar(255), surname varchar(255), age INTEGER,
                         shop_id INTEGER,
                         foreign key (shop_id) references shop.shop(id)
);

