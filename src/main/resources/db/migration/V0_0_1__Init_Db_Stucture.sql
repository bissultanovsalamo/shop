CREATE TABLE shop.products (id serial primary key, name varchar (255), made varchar(255));

insert into shop.products (name, made) values ('Танк', 'Germany');
insert into shop.products (name, made) values ('Мяч', 'England');
insert into shop.products (name, made) values ('Баскетбольный мяч', 'USA');
insert into shop.products (name, made) values ('Матрешка', 'Russia');