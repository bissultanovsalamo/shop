package kz.space.salamo.controllers;

import kz.space.salamo.dtos.UserDto;
import kz.space.salamo.services.PersonService;
import lombok.RequiredArgsConstructor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/person")
@RequiredArgsConstructor
public class UserController {
    private final PersonService personService;

    @PostMapping(path = "/create")
    public ResponseEntity<UserDto> add(@RequestBody UserDto dto) {

        return ResponseEntity.ok(personService.add(dto));

    }

    @GetMapping(path = "/all")
    public ResponseEntity<List<UserDto>> getPersons() {
        return ResponseEntity.ok(personService.getPerson());
    }

    @GetMapping(path = "/get")
    public ResponseEntity<UserDto> getId(@RequestParam(name = "id") Integer id) {
        return ResponseEntity.ok(personService.getPersonId(id));

    }

    @DeleteMapping(path = "/delete")
    public ResponseEntity<?> delete(@RequestParam(name = "id") Integer id) {
        personService.remove(id);
        return ResponseEntity.ok().build();
    }

    @PutMapping(path = "/put")
    public ResponseEntity<UserDto> putUser(@RequestBody UserDto dto) {
        return ResponseEntity.ok(personService.update(dto));
    }
}




