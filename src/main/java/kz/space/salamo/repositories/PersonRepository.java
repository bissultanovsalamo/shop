package kz.space.salamo.repositories;

import kz.space.salamo.entityes.UsersEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepository extends JpaRepository<UsersEntity, Integer> {
}
