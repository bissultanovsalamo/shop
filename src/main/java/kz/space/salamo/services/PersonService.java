package kz.space.salamo.services;
import kz.space.salamo.dtos.UserDto;
import kz.space.salamo.entityes.UsersEntity;
import kz.space.salamo.repositories.PersonRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonService {
    private final PersonRepository personRepository;
    private final ModelMapper modelMapper;

    public UserDto add(UserDto dto) {
        UsersEntity usersEntity = personRepository.save(modelMapper.map(dto, UsersEntity.class));
        return modelMapper.map(usersEntity, UserDto.class);

    }
    public List<UserDto> getPerson() {
        return personRepository.findAll().stream()
                .map(item -> modelMapper.map(item, UserDto.class))
                .collect(Collectors.toList());
    }

    public UserDto getPersonId(Integer id) {
     Optional<UsersEntity> optional = personRepository.findById(id);
     return  optional.map( item -> modelMapper.map(item, UserDto.class)).orElse(null);
    }

    public void remove(Integer id) {
        personRepository.deleteById(id);
    }

    public UserDto update(UserDto dto) {
    boolean flag =  personRepository.existsById(dto.getId());
        if(!flag){
            throw new RuntimeException("ERROR");
        }
        return this.add(dto);
    }
}
