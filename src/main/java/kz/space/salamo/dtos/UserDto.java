package kz.space.salamo.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class UserDto {
    private Integer id;
    private String name;
    private String surname;
    private Integer age;
    private Integer shop_id;

}
