package kz.space.salamo.entityes;


import lombok.Data;


import javax.persistence.*;

@Data
@Entity
@Table(name = "users", schema = "shop")
public class UsersEntity {
    @Id
    @Column(name = "id")
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "users_seq")
    @SequenceGenerator(
            name = "users_seq",
            sequenceName = "users_id_seq", schema = "shop", allocationSize = 1)
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "age")
    private Integer age;
    @Column(name = "shop_id")
    private Integer shop_id;
}
